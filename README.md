[![Latest release](https://gitlab.com/smartefact/smartefact.java.semver/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.java.semver/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.java.semver/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.java.semver/-/commits/main)
[![Coverage](https://gitlab.com/smartefact/smartefact.java.semver/badges/main/coverage.svg?key_text=Coverage&key_width=100)](https://gitlab.com/smartefact/smartefact.java.semver/-/commits/main)

# smartefact.java.semver

Java implementation of [semantic version](https://semver.org/).

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
