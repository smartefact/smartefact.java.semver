/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.semver;

import java.io.Serializable;
import java.util.ArrayList;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import smartefact.util.Characters;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import static smartefact.util.Preconditions.requireGe0;
import static smartefact.util.Preconditions.requireNotEmpty;
import smartefact.util.Strings;

/**
 * Semantic version.
 * <p>
 * A semantic version is defined by:
 * </p>
 * <ul>
 *     <li>a {@linkplain #getMajor() major}</li>
 *     <li>a {@linkplain #getMinor() minor}</li>
 *     <li>a {@linkplain #getPatch() patch}</li>
 *     <li>a {@linkplain #getPreReleases() list of pre-release labels} (may be empty)</li>
 *     <li>a {@linkplain #getBuildMetadata() list of build metadata labels} (may be empty)</li>
 * </ul>
 *
 * @author Laurent Pireyn
 * @see <a href="https://semver.org/">Semantic Versioning</a>
 */
public final class Version implements Comparable<Version>, Serializable {
    /**
     * Pre-release identifier.
     * <p>
     * This is a sealed class.
     * </p>
     *
     * @see NumericIdentifier
     * @see AlphanumericIdentifier
     */
    public abstract static class Identifier implements Comparable<Identifier>, Serializable {
        @Contract(pure = true)
        static @NotNull Identifier parseIdentifier(@NotNull String string) {
            requireNotEmpty(string);
            int value = 0;
            for (int i = 0; i < string.length(); ++i) {
                final char c = string.charAt(i);
                if (i == 0 && c == '0') {
                    // Leading zero
                    throw new IllegalArgumentException("Leading zero in string (" + string + ')');
                }
                if (value >= 0 && Characters.isDecimalDigit(c)) {
                    // The string may be a numeric identifier and c is a decimal digit
                    value = value * 10 + c - '0';
                } else {
                    // The string is not a numeric identifier, or c is not a decimal digit
                    value = -1;
                    if (!isLegalIdentifierChar(c)) {
                        throw new IllegalArgumentException("Illegal character #" + i + " in string (" + c + ')');
                    }
                }
            }
            return value >= 0 ? new NumericIdentifier(value) : new AlphanumericIdentifier(string);
        }

        @Contract(pure = true)
        static boolean isLegalIdentifierChar(char c) {
            return Characters.isAlphanumeric(c) || c == '-';
        }

        private static final long serialVersionUID = 1L;

        @Contract(pure = true)
        Identifier() {}

        @Override
        @Contract(value = "null -> false", pure = true)
        public abstract boolean equals(Object object);

        @Override
        @Contract(pure = true)
        public abstract int hashCode();

        @Override
        @Contract(pure = true)
        public abstract @NotNull String toString();
    }

    /**
     * Numeric {@link Identifier}.
     */
    public static final class NumericIdentifier extends Identifier {
        private static final long serialVersionUID = 1L;

        private final int value;

        @Contract(pure = true)
        NumericIdentifier(int value) {
            assert value >= 0;
            this.value = value;
        }

        @Contract(pure = true)
        public int getValue() {
            return value;
        }

        @Override
        @Contract(value = "null -> false", pure = true)
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof NumericIdentifier)) {
                return false;
            }
            final NumericIdentifier other = (NumericIdentifier) object;
            return value == other.value;
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return value;
        }

        @Override
        @Contract(pure = true)
        public int compareTo(@NotNull Identifier other) {
            // Comparable mandates NullPointerException
            Objects.requireNonNull(other);
            if (other instanceof AlphanumericIdentifier) {
                // The other is an alphanumeric pre-release; this is smaller
                return -1;
            }
            // The other is a numeric pre-release
            return Integer.compare(value, ((NumericIdentifier) other).value);
        }

        @Override
        @Contract(pure = true)
        public @NotNull String toString() {
            return String.valueOf(value);
        }
    }

    /**
     * Alphanumeric {@link Identifier}.
     */
    public static final class AlphanumericIdentifier extends Identifier {
        private static final long serialVersionUID = 1L;

        private final @NotNull String value;

        @Contract(pure = true)
        AlphanumericIdentifier(@NotNull String value) {
            this.value = value;
        }

        @Contract(pure = true)
        public @NotNull String getValue() {
            return value;
        }

        @Override
        @Contract(value = "null -> false", pure = true)
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof AlphanumericIdentifier)) {
                return false;
            }
            final AlphanumericIdentifier other = (AlphanumericIdentifier) object;
            return value.equals(other.value);
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        @Contract(pure = true)
        public int compareTo(@NotNull Identifier other) {
            // Comparable mandates NullPointerException
            Objects.requireNonNull(other);
            if (other instanceof NumericIdentifier) {
                // The other is a numeric pre-release; this is greater
                return 1;
            }
            // The other is an alphanumeric pre-release
            return value.compareTo(((AlphanumericIdentifier) other).value);
        }

        @Override
        @Contract(pure = true)
        public @NotNull String toString() {
            return value;
        }
    }

    /**
     * {@link Version} builder.
     *
     * @see Version#builder()
     */
    public static final class Builder {
        int major;
        int minor;
        int patch;
        final List<Identifier> preReleases = new ArrayList<>(5);
        final List<String> buildMetadata = new ArrayList<>(5);

        @Contract(pure = true)
        Builder() {}

        @Contract(value = "_ -> this", mutates = "this")
        public @NotNull Builder major(int major) {
            this.major = requireGe0(major);
            return this;
        }

        @Contract(value = "_ -> this", mutates = "this")
        public @NotNull Builder minor(int minor) {
            this.minor = requireGe0(minor);
            return this;
        }

        @Contract(value = "_ -> this", mutates = "this")
        public @NotNull Builder patch(int patch) {
            this.patch = requireGe0(patch);
            return this;
        }

        @Contract(value = "_ -> this", mutates = "this")
        private @NotNull Builder preRelease(@NotNull Identifier identifier) {
            preReleases.add(identifier);
            return this;
        }

        @Contract(value = "_ -> this", mutates = "this")
        public @NotNull Builder preRelease(int value) {
            return preRelease(new NumericIdentifier(value));
        }

        @Contract(value = "_ -> this", mutates = "this")
        public @NotNull Builder preRelease(@NotNull String string) {
            return preRelease(Identifier.parseIdentifier(string));
        }

        @Contract(value = "_ -> this", mutates = "this")
        public @NotNull Builder buildMetadata(@NotNull String string) {
            buildMetadata.add(requireNotEmpty(string));
            return this;
        }

        @Contract(value = "-> new", pure = true)
        public @NotNull Version build() {
            return new Version(this);
        }
    }

    @Language("RegExp")
    private static final String IDENTIFIERS = "[.a-zA-Z\\d-]+";
    private static final Pattern VERSION_PATTERN = Pattern.compile(
        "(\\d+)\\." + // Major
        "(\\d+)\\." + // Minor
        "(\\d+)" + // Patch
        "(-" + IDENTIFIERS + ")?" + // Pre-releases
        "(\\+" + IDENTIFIERS + ")?" // Build metadata
    );
    private static final char IDENTIFIER_DELIMITER = '.';

    private static final long serialVersionUID = 1L;

    @Contract(value = "-> new", pure = true)
    public static @NotNull Builder builder() {
        return new Builder();
    }

    @Contract(value = "_ -> new", pure = true)
    public static @NotNull Version valueOf(@NotNull String string) {
        requireNotEmpty(string);
        final Matcher matcher = VERSION_PATTERN.matcher(string);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("The string <" + string + "> is not a legal semantic version");
        }
        final Builder builder = builder()
            .major(Strings.toInt(matcher.group(1)))
            .minor(Strings.toInt(matcher.group(2)))
            .patch(Strings.toInt(matcher.group(3)));
        // Pre-releases
        String identifiers = matcher.group(4);
        if (identifiers != null) {
            for (final String identifier : parseIdentifiers(identifiers)) {
                builder.preRelease(identifier);
            }
        }
        // Build metadata
        identifiers = matcher.group(5);
        if (identifiers != null) {
            for (final String identifier : parseIdentifiers(identifiers)) {
                builder.buildMetadata(identifier);
            }
        }
        return builder.build();
    }

    @Contract(pure = true)
    private static @NotNull Iterable<String> parseIdentifiers(@NotNull String string) {
        return Strings.split(string.substring(1), IDENTIFIER_DELIMITER);
    }

    @Contract(value = "!null -> !null; null -> null", pure = true)
    public static @Nullable Version parseVersion(@Nullable String string) {
        return string != null ? valueOf(string) : null;
    }

    private final int major;
    private final int minor;
    private final int patch;
    private final @NotNull List<Identifier> preReleases;
    private final @NotNull List<String> buildMetadata;
    private final int hashCode;
    private final @NotNull String string;

    @Contract(pure = true)
    public Version(int major) {
        this(major, 0);
    }

    @Contract(pure = true)
    public Version(int major, int minor) {
        this(major, minor, 0);
    }

    @Contract(pure = true)
    public Version(int major, int minor, int patch) {
        this(major, minor, patch, emptyList(), emptyList());
    }

    @Contract(pure = true)
    private Version(
        int major,
        int minor,
        int patch,
        @NotNull List<Identifier> preReleases,
        @NotNull List<String> buildMetadata
    ) {
        this.major = requireGe0(major);
        this.minor = requireGe0(minor);
        this.patch = requireGe0(patch);
        this.preReleases = preReleases;
        this.buildMetadata = buildMetadata;
        hashCode = hashCodeBuilder()
            .property(major)
            .property(minor)
            .property(patch)
            .property(preReleases)
            // Ignore buildMetadata
            .build();
        final StringBuilder stringBuilder = new StringBuilder(100)
            .append(major)
            .append('.')
            .append(minor)
            .append('.')
            .append(patch);
        if (!preReleases.isEmpty()) {
            char separator = '-';
            for (final Identifier identifier : preReleases) {
                stringBuilder.append(separator).append(identifier);
                separator = '.';
            }
        }
        if (!buildMetadata.isEmpty()) {
            char separator = '+';
            for (final String identifier : buildMetadata) {
                stringBuilder.append(separator).append(identifier);
                separator = '.';
            }
        }
        string = stringBuilder.toString();
    }

    @Contract(pure = true)
    private Version(@NotNull Builder builder) {
        this(
            builder.major,
            builder.minor,
            builder.patch,
            unmodifiableList(builder.preReleases),
            unmodifiableList(builder.buildMetadata)
        );
    }

    @Contract(pure = true)
    public int getMajor() {
        return major;
    }

    @Contract(pure = true)
    public int getMinor() {
        return minor;
    }

    @Contract(pure = true)
    public int getPatch() {
        return patch;
    }

    @Contract(pure = true)
    public @NotNull List<Identifier> getPreReleases() {
        return preReleases;
    }

    @Contract(pure = true)
    public @NotNull List<String> getBuildMetadata() {
        return buildMetadata;
    }

    @Override
    @Contract(value = "null -> false", pure = true)
    public boolean equals(@Nullable Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Version)) {
            return false;
        }
        final Version other = (Version) object;
        return major == other.major
            && minor == other.minor
            && patch == other.patch
            && preReleases.equals(other.preReleases);
        // Ignore buildMetadata
    }

    @Override
    @Contract(pure = true)
    public int hashCode() {
        return hashCode;
    }

    @Override
    @Contract(pure = true)
    public int compareTo(@NotNull Version other) {
        // Comparable mandates NullPointerException
        Objects.requireNonNull(other);
        int result = Integer.compare(major, other.major);
        if (result != 0) {
            return result;
        }
        result = Integer.compare(minor, other.minor);
        if (result != 0) {
            return result;
        }
        result = Integer.compare(patch, other.patch);
        if (result != 0) {
            return result;
        }
        if (preReleases.isEmpty()) {
            // This has no pre-releases
            return other.preReleases.isEmpty()
                // The other has no pre-releases either; this is equal
                ? 0
                // The other has pre-releases; this is greater
                : 1;
        }
        // This has pre-releases
        if (other.preReleases.isEmpty()) {
            // The other has no pre-releases; this is smaller
            return -1;
        }
        // Compare pre-releases
        final Iterator<Identifier> preReleaseIterator = preReleases.iterator();
        final Iterator<Identifier> otherPreReleaseIterator = other.preReleases.iterator();
        while (preReleaseIterator.hasNext()) {
            if (!otherPreReleaseIterator.hasNext()) {
                // This has more pre-releases than the other; this is greater
                return 1;
            }
            result = preReleaseIterator.next().compareTo(otherPreReleaseIterator.next());
            if (result != 0) {
                // Pre-releases are different; this is different
                return result;
            }
        }
        if (otherPreReleaseIterator.hasNext()) {
            // The other has more pre-releases than this; this is smaller
            return -1;
        }
        // Ignore buildMetadata
        // This is equal to the other
        return 0;
    }

    @Override
    @Contract(pure = true)
    public @NotNull String toString() {
        return string;
    }
}
