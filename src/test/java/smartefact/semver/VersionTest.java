/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.semver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class VersionTest {
    @Test
    void getters_should_return_the_correct_values() {
        final Version version = Version.builder()
            .major(1)
            .minor(2)
            .patch(3)
            .preRelease(4)
            .preRelease("a")
            .preRelease("5")
            .buildMetadata("b")
            .buildMetadata("c")
            .build();
        assertEquals(1, version.getMajor());
        assertEquals(2, version.getMinor());
        assertEquals(3, version.getPatch());
        assertEquals(
            List.of(new Version.NumericIdentifier(4), new Version.AlphanumericIdentifier("a"), new Version.NumericIdentifier(5)),
            version.getPreReleases()
        );
        assertEquals(
            List.of("b", "c"),
            version.getBuildMetadata()
        );
    }

    @Test
    void equals_and_hashCode_should_return_correct_values() {
        final Version version1 = Version.builder()
            .major(1)
            .minor(2)
            .patch(3)
            .preRelease(4)
            .buildMetadata("a")
            .build();
        final Version version2 = Version.builder()
            .major(1)
            .minor(2)
            .patch(3)
            .preRelease(4)
            .buildMetadata("b")
            .build();
        final Version version3 = Version.builder()
            .major(1)
            .minor(2)
            .patch(3)
            .preRelease(5)
            .build();
        assertAll(() -> {
            assertTrue(version1.equals(version1));
            assertTrue(version1.equals(version2));
            assertTrue(version2.equals(version1));
            assertFalse(version1.equals(null));
            assertFalse(version1.equals(version3));
            assertFalse(version3.equals(version1));
            assertEquals(version1.hashCode(), version2.hashCode());
            // Even though the hash codes of different versions could be the same,
            // we know the implementation is good enough to produce different hash codes
            assertNotEquals(version1.hashCode(), version3.hashCode());
        });
    }

    @Test
    void compareTo_should_return_the_correct_value() {
        final List<Version> orderedVersions = Stream.of(
            "0.0.0",
            "0.0.1",
            "0.1.0",
            "0.1.1",
            "1.0.0-1",
            "1.0.0-1.1",
            "1.0.0-1.a",
            "1.0.0-1.a.b",
            "1.0.0-1.b",
            "1.0.0-2",
            "1.0.0+a",
            "1.0.1",
            "1.1.0",
            "1.1.1",
            "2.0.0"
        )
            .map(Version::valueOf)
            .collect(Collectors.toList());
        final List<Version> sortedVersions = new ArrayList<>(orderedVersions);
        Collections.sort(sortedVersions);
        assertEquals(orderedVersions, sortedVersions);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("dataVersionsAndStrings")
    void toString_should_return_the_correct_value(Version version, String string) {
        assertEquals(string, version.toString());
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("dataVersionsAndStrings")
    void valueOf_should_return_the_correct_value(Version version, String string) {
        assertEquals(version, Version.valueOf(string));
    }

    static Iterable<Arguments> dataVersionsAndStrings() {
        return List.of(
            arguments(
                new Version(1),
                "1.0.0"
            ),
            arguments(
                new Version(1, 2),
                "1.2.0"
            ),
            arguments(
                new Version(1, 2, 3),
                "1.2.3"
            ),
            arguments(
                Version.builder()
                    .major(1)
                    .preRelease(2)
                    .build(),
                "1.0.0-2"
            ),
            arguments(
                Version.builder()
                    .major(1)
                    .preRelease(2)
                    .preRelease("a")
                    .build(),
                "1.0.0-2.a"
            ),
            arguments(
                Version.builder()
                    .major(1)
                    .preRelease("a")
                    .preRelease(2)
                    .build(),
                "1.0.0-a.2"
            ),
            arguments(
                Version.builder()
                    .major(1)
                    .buildMetadata("a")
                    .build(),
                "1.0.0+a"
            ),
            arguments(
                Version.builder()
                    .major(1)
                    .buildMetadata("a")
                    .buildMetadata("b")
                    .build(),
                "1.0.0+a.b"
            ),
            arguments(
                Version.builder()
                    .major(1)
                    .preRelease(2)
                    .buildMetadata("a")
                    .preRelease("b")
                    .buildMetadata("c")
                    .build(),
                "1.0.0-2.b+a.c"
            )
        );
    }

    @ParameterizedTest
    @ValueSource(strings = {
        "-1",
        "a",
        "1.0.0-+",
        "1.0.0-.",
        "1.0.0-:",
        "1.0.0-0",
        "1.0.0-$",
        "1.0.0-a..b",
        "1.0.0++",
        "1.0.0+.",
        "1.0.0+:",
        "1.0.0+a..b",
        "1.0.0+a+b"
    })
    @NullAndEmptySource
    void valueOf_with_illegal_string_should_throw_IllegalArgumentException(String string) {
        assertThrows(
            IllegalArgumentException.class,
            () -> Version.valueOf(string)
        );
    }

    @Test
    void parseVersion_with_null_should_return_null() {
        assertNull(Version.parseVersion(null));
    }
}
